
    /* Highcharts.chart('plots', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Fruit Consumption'
        },
        xAxis: {
            categories: ['Apples', 'Bananas', 'Oranges']
        },
        yAxis: {
            title: {
                text: 'Fruit eaten'
            }
        },
        series: [{
            name: 'Jane',
            data: [1, 0, 4]
        }, {
            name: 'John',
            data: [5, 7, 3]
        }]
    });









    console.log(Highcharts)*/
  /*Highcharts.chart('plots', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Number Of Matches Per Year'
        },
       
        xAxis: {
            categories: ['2008', '2009','2010', '2011', '2012','2013','2014','2015','2016','2017'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 1,
            title: {
                text: 'Matches played in a Year',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
    
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: '2008',
            data: [58]
        }, {
            name: '2009',
            data: [57]
        }, {
            name: '2010',
            data: [60]
        }, {
            name: '2011',
            data: [73]
        }, {
            name: '2012',
            data: [74]
        }, {
            name: '2013',
            data: [76]
        }, {
            name: '2014',
            data: [60]
        }, {
            name: '2015',
            data: [59]
        }, {
            name: '2016',
            data: [60]
        }, {
            name: '2017',
            data: [59]
        }]
    })

*/


/*fetch(`/NumberOfmatchesPerYear.json`)
.then(data => data.json())
.then(data => {
    // console.log(data);
    const years = Object.entries(data);
    // console.log(years)
    //const matches = Object.values(data);
    // console.log(matches)

    Highcharts.chart('plots', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Matches played per year'
        },
        xAxis: {
            categories: years,
            title: {
                text: null
            }
        },
        yAxis: {
            
            title: {
                text: 'No. of matches'
            },
            labels: {
                overflow: 'justify'
            }
        },
        series: [{
            name:"Matches",
            data:matches
        }]
    });

}) */




            
            
            
   
    Highcharts.chart('matches', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Number Of Matches played per Year'
        },
       
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of matches'
            }
        },
        legend: {
            enabled: false
        },
        
        series: [{
            name: 'Matches',
            data: [
                
                   ["2008", 58],
                    ["2009", 57],
                    ["2010", 60],
                    ["2011", 73],
                    ["2012", 74],
                    ["2013", 76],
                    ["2014", 60],
                    ["2015", 59],
                    ["2016", 60],
                    ["2017", 59]                
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });


             
            
            
   
    Highcharts.chart('toss', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Matches and toss won in all seasons'
        },
        
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of matches and toss won'
            }
            
        },
        legend: {
            enabled: false
        },
        
        series: [{
            name: '',
            data: [
                
                
                    ["Royal Challengers Bangalore", 5],
                    ["Rising Pune Supergiant", 5],
                    ["Kolkata Knight Riders",1],
                    ["Kings XI Punjab", 1],
                    ["Sunrisers Hyderabad", 3],
                    ["Mumbai Indians", 1],
                    ["Gujarat Lions", 5],
                    ["Delhi Daredevils", 2],
                    ["Chennai Super Kings", 1],
                    ["Rajasthan Royals", 2],
                    ["Deccan Chargers", 1],
                    ["Kochi Tuskers Kerala", 2],
                    ["Pune Warriors", 2],
                    ["Rising Pune Supergiants", 2]
                            
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });



    Highcharts.chart('extra_runs', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Extra Runs Per team in 2016'
        },
        
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Extra runs'
            }
            
        },
        legend: {
            enabled: false
        },
        
        series: [{
            name: 'Extra runs',
            data: [
                ["Rising Pune Supergiants", 108],
                ["Mumbai Indians", 102],
                ["Kolkata Knight Riders", 122],
                ["Delhi Daredevils", 106],
                ["Gujarat Lions", 98],
                ["Kings XI Punjab",100],
                ["Sunrisers Hyderabad", 107],
                ["Royal Challengers Bangalore", 156]
                
                  
                            
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

    Highcharts.chart('MoM', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Most man of the match awards'
        },
        
        xAxis: {
            categories: [''
               
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Man Of the match'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Chris Gayle',
            data:[18]
    
        }]
    });
        
    