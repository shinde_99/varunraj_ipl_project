 import csv from 'csvtojson'
 import fs from 'fs'
 import path from 'path'
 import {fileURLToPath} from 'url'

const __dirname =path.dirname(fileURLToPath(import.meta.url))

const matchFilePath='./data/matches.csv'
const deliveriesPath='./data/deliveries.csv'

//console.log(path.resolve(__dirname,JsonFilePath),JSON.stringify(CSV_data)

const modifyPath = filePath =>filePath.replace('.csv','.json')

async function convertCSVtoJSON(csvFilePath){
    const CSV_data= await csv().fromFile(path.resolve(__dirname,csvFilePath))
    const outFilePath =modifyPath(csvFilePath)
    fs.writeFileSync(path.resolve(__dirname,outFilePath),JSON.stringify(CSV_data))
        console.log(outFilePath)
}
convertCSVtoJSON(matchFilePath)
convertCSVtoJSON(deliveriesPath)