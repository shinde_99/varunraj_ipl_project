import  path  from 'path';
import { fileURLToPath } from "url";
import fs from 'fs'
import ipl from './ipl.js'

const __dirname =path.dirname(fileURLToPath(import.meta.url))

const matchFilePath ='../data/matches.json'
const deliveriesFilePath='../data/deliveries.json'

const matchData=JSON.parse(fs.readFileSync(path.resolve(__dirname,matchFilePath)))
const deliveryData=JSON.parse(fs.readFileSync(path.resolve(__dirname,deliveriesFilePath)))


const exportToJson = (fileName,data)=>{
    const outFolderPath ='../public/output'
    const outFilePath=path.resolve(__dirname,outFolderPath,`${fileName}.json`)
    fs.writeFileSync(outFilePath,JSON.stringify(data))
}



const data =ipl.NumberOfmatchesPerYear(matchData)
exportToJson('NumberOfmatchesPerYear',data)

const data_2 =ipl.matchesWonPerTeamPerYear(matchData)
exportToJson('matchesWonPerTeamPerYear',data_2)

const data_3 = ipl.extraRunsPerTeam(matchData,deliveryData)
exportToJson('extraRunsPerTeam',data_3)


const data_4=ipl.economicalBowler(matchData,deliveryData)
exportToJson('economicalBowler',data_4)

const data_5=ipl.MatchesandTossWon(matchData)
exportToJson('MatchesandTossWon',data_5)

const data_6=ipl.PlayerOfTheMatch(matchData)
exportToJson('PlayerOfTheMatch',data_6)

