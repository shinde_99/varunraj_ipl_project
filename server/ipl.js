


   
 // 1. Number of matches played per year of all years 
  
const NumberOfmatchesPerYear=(matchData)=> 
    
    {
 
        let NumberOFmatches = matchData.reduce((result, currVal) => 
        {
            if (result[currVal.season]) 
            {
                result[currVal.season] = result[currVal.season] + 1;
            } 
            else 
            {
                result[currVal.season] = 1;
            }
            return result;
        }, {})
        // console.log(NumberOfmatches)

          return NumberOFmatches;
    }  
//export default {NumberOfmatchesPerYear};

   
// 2. Number of matches of won per team.
const matchesWonPerTeamPerYear= (matchData)=>
{
    const matchesWon = matchData.reduce((result,currVal)=>{
    
    if(result.hasOwnProperty(currVal.season))
    {
        if(result[currVal.season].hasOwnProperty(currVal.winner))
        {
            result[currVal.season][currVal.winner]=result[currVal.season][currVal.winner]+1;
        }
        else
        {
            result[currVal.season][currVal.winner]=1;
        }
    }
    else
    {
        result[currVal.season]={};
        result[currVal.season][currVal.winner]=1;
    }
    return result;
},{})
 return matchesWon;
}

//export default matchesWonPerTeamPerYear;

// 3. Extra runs per team

const  extraRunsPerTeam=(matchData,deliveryData) => {
    //Filtering all the 2016 ID's and mapping them
    const matchIds2016= matchData.filter(match=>match.season==='2016').map(match=>match.id)
    //console.log(matchIds2016);

    const deliveryData2016=deliveryData.filter(
        eachDelivery => matchIds2016.indexOf(eachDelivery.match_id)!==-1
    )


  //console.log(deliveryData2016)

  const runsEachTeam=deliveryData2016.reduce((runsEachTeam,currDelivery)=>{
        const bowlingTeam =currDelivery['bowling_team']
        

        if(runsEachTeam[bowlingTeam])
        {
            runsEachTeam[bowlingTeam] += +currDelivery['extra_runs']
        }
        else
        {
            runsEachTeam[bowlingTeam]= +currDelivery['extra_runs']
        }
         return runsEachTeam
         //console.log(runsEachTeam);
    },{})
         return runsEachTeam
}
    //export default {extraRunsPerTeam}

    // 4.Top 10 Economical bowler

        const economicalBowler = (matchData,deliveryData) =>{
        const matchIDs2015 = matchData.filter(match=>match.season==='2015').map(match=>match.id)
        //console.log(matchIDs2015)

       const deliveryData2015 = deliveryData.filter(
           everyDelivery => matchIDs2015.indexOf(everyDelivery.match_id)!==-1

       )
        //console.log(deliveryData2015[0]);
        const runsEachTeam=deliveryData2015.reduce((runsEachTeam,currDelivery)=>{
            const bowlingTeam =currDelivery['bowler']
            if(runsEachTeam[bowlingTeam])
            {
                runsEachTeam[bowlingTeam]+= +currDelivery['total_runs']
            }
            else
            {
                runsEachTeam[bowlingTeam]= +currDelivery['total_runs']
            }
             return runsEachTeam
             //console.log(runsEachTeam);
        },{})
             return Object.entries(runsEachTeam).sort((a,b) => b[1]-a[1]).map(element=>element[0])
}
  

   
// 5 Find the number of times each team won the toss and also won the match

const MatchesandTossWon=(matchData)=> 
    
    {
 
        var NumberOfTossWon = matchData.reduce((result, currVal) => 
        {
            if (result[currVal.toss_winner]==result[currVal.winner]) 
            {
                result[currVal.toss_winner] = result[currVal.toss_winner] + 1;
            } 
            else 
            {
                result[currVal.toss_winner] = 1;
            }
            return result;
        }, {})
        // console.log(NumberOfTossWon)

          return NumberOfTossWon;
    } 
    const PlayerOfTheMatch =(matchData) =>{
        var ManOfTheMatch = matchData.reduce((result,currVal)=>{
            
            if(result[currVal.player_of_match])
            {
                result[currVal.player_of_match]+=1;
            }
            else
            {
                result[currVal.player_of_match]=1;
            }
            return result;
            
     } ,{})  
     
     let result = Object.entries(ManOfTheMatch).sort((a,b) => b[1]-a[1]);
     return result[0];
    }
    
    export default{economicalBowler,
        NumberOfmatchesPerYear,
        matchesWonPerTeamPerYear,
        extraRunsPerTeam,
        MatchesandTossWon,
        PlayerOfTheMatch
    }