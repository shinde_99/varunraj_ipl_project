import http from 'http';
import path from 'path';
import fs from 'fs';
import {fileURLToPath} from 'url';


const __dirname =path.dirname(fileURLToPath(import.meta.url))
const server = http.createServer((request, response)=> {
    const reqUrl =request.url
    const requrlFileExt=request.url.split('.').splice(-1)[0]
    const clientFolderPath =path.resolve(__dirname,'../client')
    const outputFolderPath=path.resolve(__dirname,'../public/output')
    //console.log(requrlFileExt)

    switch (requrlFileExt){
        case'/':
        const htmlFilePath=path.resolve(__dirname,"../client/index.html")
        fs.readFile(htmlFilePath,'utf-8',(err,htmlData)=>{
            if(err)
            {
                response.writeHead(404)
            }
            else
            {
                response.writeHead(200,{
                    "Content-Type":"text/html"
                })
                response.end(htmlData)
            }
        })
        break;

        case'js':
        const jsFilePath=path.join(clientFolderPath,reqUrl)
        fs.readFile(jsFilePath,'utf-8',(err,jsData)=>{
            if(err)
            {
                response.writeHead(404)
                console.log(err)
                response.end(err)
            }
            else
            {
                response.writeHead(200,{
                    "Content-Type":"application/javascript"
                })
                response.end(jsData)
            }
        })

        break;

        case'json':
        const jsonFilePath=path.join(outputFolderPath,reqUrl)
        fs.readFile(jsonFilePath,'utf-8',(err,jsonData)=>{
            if(err)
            {
                response.writeHead(404)
                console.log(err)
            }
            else
            {
                response.writeHead(200,{
                    'Content-Type':"application/json"
                })
                response.end(jsonData)
            }
        })   
        break;

    }
})

server.listen(5000);